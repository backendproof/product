package com.myproject.product.controller;

import com.myproject.product.model.Product;
import com.myproject.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @GetMapping(path = "/add")
    public @ResponseBody String addNewProduct (@RequestParam String category,
                                                @RequestParam String code,
                                                @RequestParam String detail,
                                               @RequestParam String name,
                                               @RequestParam String price) {
        Product product = new Product();
        product.setCategory(category);
        product.setCode(code);
        product.setDetail(detail);
        product.setName(name);
        product.setPrice(Double.parseDouble(price));

        productRepository.save(product);
        return "Saved";
    }

    @GetMapping(path = "/remove")
    public @ResponseBody String removeProduct(@RequestParam int id) {
        productRepository.delete(id);
        return "Removed";
    }
}
